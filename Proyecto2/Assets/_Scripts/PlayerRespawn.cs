using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class PlayerRespawn : MonoBehaviour
{

    public Animator animator;

    private float checkPointPositionX, checkPointPositionY;
    private int playerLives = 6; // N�mero de vidas del jugador

    void Start()
    {
        if (PlayerPrefs.GetFloat("checkPointPositionX") != 0)
        {
            transform.position = new Vector2(PlayerPrefs.GetFloat("checkPointPositionX"), PlayerPrefs.GetFloat("checkPointPositionY"));
        }
    }

    public void ReachedCheckPoint(float x, float y)
    {
        PlayerPrefs.SetFloat("checkPointPositionX", x);
        PlayerPrefs.SetFloat("checkPointPositionY", y);
    }

    public void PlayerDamage()
    {
        playerLives--; // Reduce una vida cuando el jugador recibe da�o
        animator.Play("Hit");

        if (playerLives <= 0)
        {
            // Si el jugador se queda sin vidas, reinicia el nivel
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
