using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class CoinManager : MonoBehaviour
{
    public Text totalCoins;
    public Text collectedCoins;
    private int totalCoinsInLevel;

    private void Start()
    {
        totalCoinsInLevel = transform.childCount;

    }
    private void Update()
    {
        AllFruitsCollected();
        totalCoins.text = totalCoinsInLevel.ToString();
        collectedCoins.text = transform.childCount.ToString();
    }

    public void AllFruitsCollected()
    {
        if (transform.childCount == 0)
        {
            Debug.Log("No quedan monedas, Victoria");

            Invoke("ChangeScene", 1);
        }
    }

    void ChangeScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
