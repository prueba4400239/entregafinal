using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalScene : MonoBehaviour
{

    public void Volver()
    {
        // Cargar la escena "_MainMenu"
        SceneManager.LoadScene("_MainMenu");
    }

    public void Salir()
    {
        Debug.Log("Salir");
        Application.Quit();
    }
}
