using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //        NORMAL JUMP
    public float runSpeed = 2;
    public float jumpSpeed = 3;

    //        DOUBLE JUMP
    public float doubleJumpSpeed = 3.5f;
    private bool canDoubleJump;

    //        REFERENCES
    Rigidbody2D rb2D;

    //VARIABLES FOR BETTER JUMP 
    public bool betterJump = false;
    public float falMultiplier = 0.2f;
    public float lowJumpMultiplier = 2f;

    // ASIGNMENT IN INSPECTOR
    public SpriteRenderer spriteRenderer;
    public Animator animator;

    // AUDIO
    private AudioSource audioSource;
    [SerializeField] private AudioClip jumpSound;
    [SerializeField] private AudioClip doubleJumpSound;

    public GameObject dustLeft;
    public GameObject dustRight;

    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>(); // A�adido para obtener el componente de audio
    }

    private void Update()
    {
        //             SALTO (Modificado para DoubleJump)
        if (Input.GetKeyDown("space"))
        {
            if (CheckGround.isGrounded)
            {
                canDoubleJump = true;
                rb2D.velocity = new Vector2(rb2D.velocity.x, jumpSpeed);
                PlayJumpSound(); // Reproduce el sonido del salto
            }
            else
            {
                if (canDoubleJump)
                {
                    rb2D.velocity = new Vector2(rb2D.velocity.x, doubleJumpSpeed);
                    canDoubleJump = false;
                    PlayDoubleJumpSound(); // Reproduce el sonido del doble salto
                }
            }
        }

        if (CheckGround.isGrounded == false)
        {
            animator.SetBool("Jump", true); //Saltara la animacion
            animator.SetBool("Run", false); //Saltara la animacion
        }

        if (CheckGround.isGrounded == true)
        {
            animator.SetBool("Jump", false); //No saltara la animacion
            animator.SetBool("Fall", false); //No saltara la animacion
        }

        if (rb2D.velocity.y < 0)
        {
            animator.SetBool("Fall", true);
        }
        else if (rb2D.velocity.y > 0)
        {
            animator.SetBool("Fall", false);
        }
    }

    void FixedUpdate()
    {
        //                      MOVEMENT 
        if (Input.GetKey("d") || Input.GetKey("right"))
        {
            rb2D.velocity = new Vector3(runSpeed, rb2D.velocity.y);
            spriteRenderer.flipX = false;
            animator.SetBool("Run", true);
            if (CheckGround.isGrounded == true) 
            {
                dustLeft.SetActive(true);
                dustRight.SetActive(false);
            }
        }
        else if (Input.GetKey("a") || Input.GetKey("left"))
        {
            rb2D.velocity = new Vector3(-runSpeed, rb2D.velocity.y);
            spriteRenderer.flipX = true;
            animator.SetBool("Run", true);
            if (CheckGround.isGrounded == true)
            {
                dustLeft.SetActive(false);
                dustRight.SetActive(true);
            }
        }
        else
        {
            rb2D.velocity = new Vector3(0, rb2D.velocity.y);
            animator.SetBool("Run", false);
            dustLeft.SetActive(false);
            dustRight.SetActive(false);
        }

        if (betterJump)
        {
            if (rb2D.velocity.y < 0)
            {
                rb2D.velocity += Vector2.up * Physics2D.gravity.y * (falMultiplier) * Time.deltaTime;
            }
            if (rb2D.velocity.y > 0 && !Input.GetKey("space"))
            {
                rb2D.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier) * Time.deltaTime;
            }
        }
    }

    // M�todos para reproducir sonidos
    private void PlayJumpSound()
    {
        if (jumpSound != null)
        {
            audioSource.PlayOneShot(jumpSound);
        }
    }

    private void PlayDoubleJumpSound()
    {
        if (doubleJumpSound != null)
        {
            audioSource.PlayOneShot(doubleJumpSound);
        }
    }

}
