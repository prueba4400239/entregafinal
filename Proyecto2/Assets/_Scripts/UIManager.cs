using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class UIManager : MonoBehaviour
{
    public GameObject optionsPanel;
    public AudioSource click;

    void Update()
    {
        // Check if the Esc key is pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Toggle the options panel
            if (optionsPanel.activeSelf)
            {
                Return();
            }
            else
            {
                OptionsPanel();
            }
        }
    }

    public void OptionsPanel()
    {
        Time.timeScale = 0f;
        optionsPanel.SetActive(true);
    }

    public void Return()
    {
        Time.timeScale = 1.0f;
        optionsPanel.SetActive(false);
    }

    public void GoMainMenu()
    {
        SceneManager.LoadScene("_MainMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void PlaySoundButton() 
    { 
        click.Play();
    }
}
