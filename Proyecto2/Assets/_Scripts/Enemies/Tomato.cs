using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tomato : MonoBehaviour
{
    public Transform player;         // Referencia al transform del Player
    public float[] jumpForces;       // Array de fuerzas de salto
    public float jumpInterval = 2f;  // Intervalo de tiempo entre cada salto

    private Rigidbody2D rb;
    private SpriteRenderer spriteRenderer;
    private float nextJumpTime;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        nextJumpTime = Time.time;

        // Si no se han asignado fuerzas de salto, asignar valores predeterminados
        if (jumpForces == null || jumpForces.Length == 0)
        {
            jumpForces = new float[] { 1f, 3f, 5f };
        }

        // Configurar el Rigidbody2D para no permitir movimiento en X y rotaci�n
        rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
    }

    void Update()
    {
        // Hacer que el GameObject salte a intervalos regulares
        if (Time.time >= nextJumpTime)
        {
            Jump();
            nextJumpTime = Time.time + jumpInterval;
        }

        // Voltear el Sprite para mirar al Player
        FlipTowardsPlayer();
    }

    void Jump()
    {
        float randomJumpForce = GetRandomJumpForce();
        rb.velocity = new Vector2(0, randomJumpForce);  // Solo permitir movimiento en Y
    }

    float GetRandomJumpForce()
    {
        int randomIndex = Random.Range(0, jumpForces.Length);
        return jumpForces[randomIndex];
    }

    void FlipTowardsPlayer()
    {
        if (player != null)
        {
            if (player.position.x < transform.position.x)
            {
                spriteRenderer.flipX = true;
            }
            else
            {
                spriteRenderer.flipX = false;
            }
        }
    }
}
