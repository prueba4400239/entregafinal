using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JumpEnemyAttack : MonoBehaviour
{

    [Header("For Patrolling")]
    [SerializeField] float moveSpeed = 2f;
    private float moveDirection = 0.2f;
    private bool facingRight = true;
    [SerializeField] Transform wallCheckPoint;
    [SerializeField] float circleRadius = 0.2f;
    [SerializeField] LayerMask groundLayer;
    private bool checkingWall;

    [Header("For JumpAttack")]
    [SerializeField] float jumpHeight;
    [SerializeField] Transform player;
    [SerializeField] Transform groundCheck;
    [SerializeField] Vector2 boxSize;
    private bool isGrounded;

    [Header("For SeeingPlayer")]
    [SerializeField] Vector2 lineOfSite;
    [SerializeField] LayerMask playerLayer;
    private bool canSeePlayer;

    [Header("Other")]
    private Rigidbody2D enemyRB;
    private int enemyLives = 3; // N�mero de vidas del enemigo

    [SerializeField] float attackDelay = 1.5f; // Tiempo de espera antes de atacar
    private bool canAttack = true; // Bandera para controlar si el enemigo puede atacar

    private AudioSource audioSource; // Referencia al componente AudioSource

    void Start()
    {
        enemyRB = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>(); // Obtener el componente AudioSource del enemigo
        StartCoroutine(AttackCoroutine());
    }

    IEnumerator AttackCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(attackDelay);

            checkingWall = Physics2D.OverlapCircle(wallCheckPoint.position, circleRadius, groundLayer);
            isGrounded = Physics2D.OverlapBox(groundCheck.position, boxSize, 0, groundLayer);
            canSeePlayer = Physics2D.OverlapBox(transform.position, lineOfSite, 0, playerLayer);

            if (canSeePlayer && isGrounded)
            {
                JumpAttack();
            }
            else if (isGrounded)
            {
                Patrolling();
            }

            FlipTowardsPlayer();
        }
    }

    void Patrolling()
    {
        if (checkingWall)
        {
            Flip();
        }
        enemyRB.velocity = new Vector2(moveSpeed * moveDirection, enemyRB.velocity.y);
    }

    void JumpAttack()
    {
        float distanceFromPlayer = player.position.x - transform.position.x;
        if (isGrounded)
        {
            enemyRB.AddForce(new Vector2(distanceFromPlayer, jumpHeight), ForceMode2D.Impulse);
        }
    }

    void FlipTowardsPlayer()
    {
        float playerPosition = player.position.x - transform.position.x;
        if (playerPosition < 0 && facingRight)
        {
            Flip();
        }
        else if (playerPosition > 0 && !facingRight)
        {
            Flip();
        }
    }

    void Flip()
    {
        moveDirection *= -1;
        facingRight = !facingRight;
        transform.Rotate(0, 180, 0);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(wallCheckPoint.position, circleRadius);

        Gizmos.color = Color.green;
        Gizmos.DrawCube(groundCheck.position, boxSize);

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, lineOfSite);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            // Reducir las vidas del enemigo cuando el jugador lo salte
            enemyLives--;

            // Reproducir el sonido cuando el enemigo pierde una vida
            if (audioSource != null)
            {
                audioSource.Play();
            }

            // Verificar si el enemigo ha perdido todas sus vidas
            if (enemyLives <= 0)
            {
                // Cargar la siguiente escena cuando el enemigo es destruido
                SceneManager.LoadScene("Level4");
            }
        }
    }
}
