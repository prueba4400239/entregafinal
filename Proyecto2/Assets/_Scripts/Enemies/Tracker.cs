using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tracker : MonoBehaviour
{
    //      REFERENCES
    public Animator animator;
    public SpriteRenderer spriteRenderer;

    //      VARIABLES
    public float speed = 0.5f; //
    public float waitTime; //
    public float startWaitTime = 2; //El tiempo que pasara estando en un punto

    public Transform[] moveSpots;

    private int i = 0;
    private Vector2 actualPosition; //Nuestra posicion actual

    private void Start()
    {
        waitTime = startWaitTime;
    }

    private void Update()
    {
        StartCoroutine(CheckEnemyMoving()); //Empieza la corrutina

        transform.position = Vector2.MoveTowards(transform.position, moveSpots[i].transform.position, speed * Time.deltaTime);

        if (Vector2.Distance(transform.position, moveSpots[i].transform.position) < 0.1f)
        {
            if (waitTime <= 0)
            {
                if (moveSpots[i] != moveSpots[moveSpots.Length - 1])
                {
                    i++;
                }
                else
                {
                    i = 0;
                }
                waitTime = startWaitTime;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }

        IEnumerator CheckEnemyMoving()
        {
            actualPosition = transform.position;
            yield return new WaitForSeconds(0.2f);

            if (transform.position.x > actualPosition.x)
            {
                spriteRenderer.flipX = false;
            }
            else if (transform.position.x < actualPosition.x)
            {
                spriteRenderer.flipX = true;
            }
        }
    }
}
