using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    // REFERENCIAS
    public Animator animator;  // Referencia al componente Animator
    public BoxCollider2D boxCollider;  // Referencia al componente BoxCollider2D

    // VARIABLES
    public float activationInterval = 5f;  // Intervalo en segundos entre cada activaci�n
    public float animationDuration = 2f;  // Duraci�n en segundos de la animaci�n

    private void Start()
    {
        // Iniciar la corrutina que activar� la animaci�n y el collider en intervalos regulares
        StartCoroutine(ActivateAndDeactivate());
    }

    private IEnumerator ActivateAndDeactivate()
    {
        while (true)
        {
            // Activar la animaci�n y el collider
            animator.SetBool("FireOn", true);
            boxCollider.enabled = true;

            // Esperar la duraci�n de la animaci�n
            yield return new WaitForSeconds(animationDuration);

            // Desactivar la animaci�n y el collider (asumo que hay un par�metro booleano llamado "Idle" o "Off")
            animator.SetBool("FireOn", false);

            boxCollider.enabled = false;

            // Esperar el intervalo de tiempo especificado antes de volver a activar
            yield return new WaitForSeconds(activationInterval);
        }
    }
}
